angular.module('your_app_name.controllers', [])

.controller('AuthCtrl', function($scope, $ionicConfig) {

})

// APP
.controller('AppCtrl', function($scope, $ionicConfig, $window) {

    $scope.user_name = $window.localStorage.user_name;
})

//LOGIN
.controller('LoginCtrl', function($scope, $state, $templateCache, $q, $rootScope, $http, $ionicPopup, $window) {
    // $scope.doLogIn = function(){
    //  $state.go('app.feeds-categories');
    // };

    $scope.user = {};

    $scope.user.roll_no = "";
    $scope.user.pin = "12345";

    // We need this for the form validation
    $scope.selected_tab = "";

    $scope.$on('my-tabs-changed', function(event, data) {
        $scope.selected_tab = data.title;
    });


    $scope.doLogIn = function() {
        $state.go('app.feeds-categories');
        /*        
                //$state.go('app.feeds-categories');
                if (typeof($scope.user.roll_no) === 'undefined') {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Roll number cannot be empty!',
                        template: 'Please enter your valid roll number.'
                    });
                } else {
                    $url = "http://digitalpartnering.com/server/v1/api.php?request=signin&roll_no=" + $scope.user.roll_no;
                    // $ionicLoading.show({
                    //         template: "Loading..."
                    //     })

                    $http.get($url)
                        .success(function(response) {
                            console.log("Received Login data via HTTP");
                            $scope.res = response;
                            if (response.status) {
                                 

                                $window.localStorage.user_name = response.data[0].user_name;
                                $window.localStorage.user_roll_no = response.data[0].roll_no;
                                $window.localStorage.user_email = response.data[0].email_id;
                                $window.localStorage.user_pin = response.data[0].pincode;
                                $window.localStorage.user_mobile = response.data[0].mobile_no;

                                $state.go('app.feeds-categories');

                            } else {
                              
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Login failed!',
                                    template: 'Roll Number Not exists. Kindly signup as new.'
                                });
                            }
                        })
                        .error(function() {
                            console.log("Error while making HTTP call login data");
                                // $ionicLoading.hide();
                                // deferred.resolve({ "showErrorMsg": "No internet connection! Make sure you are connected to the internet and retry" });
                        });




                }
        */
    };

})

.controller('SignupCtrl', function($scope, $state, $ionicPopup, $window) {
    $scope.user = {};

    $scope.user.email = "";
    //$scope.user.roll_no = "";

    $scope.doSignUp = function() {
        $state.go('app.feeds-categories');
        // if (($scope.user.email !== '')) {
        //     $window.localStorage.user_email = $scope.user.email;
        //     $state.go('app.add-profile');
        // } else {
        //     var alertPopup = $ionicPopup.alert({
        //         title: 'Error!',
        //         template: "Email id can't be empty."
        //     });
        // }



    };



})

.controller('EditProfile', function($scope, $state, $ionicPopup, $window) {

    $scope.user_name = $window.localStorage.user_name;
    $scope.roll_no = $window.localStorage.user_roll_no;
    $scope.email_id = $window.localStorage.user_email;
    $scope.pincode = $window.localStorage.user_pin;
    $scope.mobile = $window.localStorage.user_mobile;

})

.controller('Profile', function($scope, $state, $ionicPopup, $window) {

    $scope.user_name = $window.localStorage.user_name;
    $scope.roll_no = $window.localStorage.user_roll_no;
    $scope.email_id = $window.localStorage.user_email;
    $scope.pincode = $window.localStorage.user_pin;
    $scope.mobile = $window.localStorage.user_mobile;

})

.controller('ForgotPasswordCtrl', function($scope, $state) {
    $scope.recoverPassword = function() {
        $state.go('app.feeds-categories');
    };

    $scope.user = {};
})

.controller('AddProfileCtrl', function($scope, $state, $ionicPopup, $http, $window) {
    $scope.user = {};
    $scope.user.roll_no = "";
    $scope.user.name = "";
    $scope.user.password = "";
    $scope.user.retype_password = "";
    $scope.user.pincode = "";
    $scope.user.mobile = "";
    $url = "http://digitalpartnering.com/server/v1/api.php";

    $scope.addProfile = function() {
        var alertPopup = "";
        if ($scope.user.roll_no === '') {
            alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: "Roll Number can't be empty."
            });
        } else if ($scope.user.name === '') {
            alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: "User Name can't be empty."
            });
        } else if ($scope.user.password === '') {
            alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: "Password can't be empty."
            });
        } else if ($scope.user.retype_password === '') {
            alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: "Retype Password can't be empty."
            });
        } else if ($scope.user.pincode === '') {
            alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: "Pincode can't be empty."
            });
        } else if ($scope.user.mobile === '') {
            alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: "Mobile Number can't be empty."
            });
        } else {
            if ($scope.user.password !== $scope.user.retype_password) {
                alertPopup = $ionicPopup.alert({
                    title: 'Error!',
                    template: "Password did't match."
                });
            } else {

                $window.localStorage.user_name = $scope.user.name;
                $window.localStorage.user_pin = $scope.user.pincode;
                $window.localStorage.user_mobile = $scope.user.mobile;
                $window.localStorage.user_roll_no = $scope.user.roll_no;


                myobject = {
                    request: 'signup',
                    user_name: $scope.user.name,
                    user_type: "student",
                    roll_no: $scope.user.roll_no,
                    email_id: $window.localStorage.user_email,
                    mobile_no: $scope.user.mobile,
                    password: $scope.user.password,
                    pincode: $scope.user.pincode,

                };

                Object.toparams = function ObjecttoParams(obj) {
                    var p = [];
                    for (var key in obj) {
                        p.push(key + '=' + encodeURIComponent(obj[key]));
                    }
                    return p.join('&');
                };

                var req = {
                    method: 'POST',
                    url: $url,
                    data: Object.toparams(myobject),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                };

                $http(req).success(function(data, status, headers, config) {
                    if (data.status === "Success") {
                        $state.go('app.feeds-categories');
                    }
                }).error(function(data, status, headers, config) {
                    alertPopup = $ionicPopup.alert({
                        title: 'Oops!',
                        template: "Something went wrong. Please try again after sometimes"
                    });
                    //ionicToast.show("No internet connection! Make sure you are connected to the internet and retry", 'bottom', true, 2500);
                });
            }
        }
    };

})



// .controller('SendMailCtrl', function($scope) {
//     $scope.sendMail = function() {
//         cordova.plugins.email.isAvailable(
//             function(isAvailable) {
//                 // alert('Service is not available') unless isAvailable;
//                 cordova.plugins.email.open({
//                     to: 'envato@startapplabs.com',
//                     cc: 'hello@startapplabs.com',
//                     // bcc:     ['john@doe.com', 'jane@doe.com'],
//                     subject: 'Greetings',
//                     body: 'How are you? Nice greetings from IonFullApp'
//                 });
//             }
//         );
//     };
// })

.controller('MapsCtrl', function($scope, $ionicLoading, $http, $interval) {

    // Initialize the Google Maps API v3
    $scope.map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ]
    });

    $scope.marker = null;
    $scope.lat = 0;
    $scope.lon = 0;
    $ionicLoading.show({
        template: "Loading..."
    })
    $scope.autoUpdate = function() {
        $url = "http://gpstracker.net.in/gs/api/api.php?api=user&ver=1.0&key=556BEC5D3FE6AB1&cmd=OBJECT_GET_LOCATIONS,355488020515664";
        

        $http.get($url)
            .success(function(response) {
                console.log("Received Login data via HTTP");
                $scope.res = response;
                $scope.lat = $scope.res['355488020515664']['lat'];
                $scope.lon = $scope.res['355488020515664']['lng'];
                $ionicLoading.hide();
            })
            .error(function() {
                console.log("Error while making HTTP call login data");
                // $ionicLoading.hide();
                // deferred.resolve({ "showErrorMsg": "No internet connection! Make sure you are connected to the internet and retry" });
            });

        var newPoint = new google.maps.LatLng($scope.lat,
            $scope.lon);
        // myMarker.setMap(null).
        if ($scope.marker) {
            // Marker already created - Move it
            $scope.marker.setPosition(newPoint);
        } else {
            // Marker does not exist - Create it
            $scope.marker = new google.maps.Marker({
                position: newPoint,
                map: $scope.map,
                icon: {
                    fillColor: '#f5861f',
                    path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                    strokeColor: "#f5861f",
                    scale: 6
                      }
            });
        }
        
        // Center the map on the new position
        $scope.map.setCenter(newPoint);

        // Call the autoUpdate() function every 5 seconds
        setTimeout($scope.autoUpdate, 11000);
    };

    $scope.autoUpdate();

})

.controller('Refresh', function($scope, $interval, $http) {
    var count = 0; // this is default cont value.

    $scope.displayMsg = "This is auto Refreshed " + count + " counter time.";

    // The  $interval function is used to auto refresh the count div.
    var auto = $interval(function() {
        $url = "http://gpstracker.net.in/gs/api/api.php?api=user&ver=1.0&key=556BEC5D3FE6AB1&cmd=OBJECT_GET_LOCATIONS,355488020515664";
        // $ionicLoading.show({
        //         template: "Loading..."
        //     })

        $http.get($url)
            .success(function(response) {
                console.log("Received Login data via HTTP");
                $scope.res = response;
                console.log($scope.res['355488020515664']['lat']);
                console.log($scope.res['355488020515664']['lng']);
                $scope.center_position = {
                    lat: $scope.res['355488020515664']['lat'],
                    lng: $scope.res['355488020515664']['lng']
                };
                $scope.current_position = {
                    lat: $scope.res['355488020515664']['lat'] + 100,
                    lng: $scope.res['355488020515664']['lng']
                };
            })
            .error(function() {
                console.log("Error while making HTTP call login data");
                // $ionicLoading.hide();
                // deferred.resolve({ "showErrorMsg": "No internet connection! Make sure you are connected to the internet and retry" });
            });
    }, 100);
})

// FEED
//brings all feed categories
.controller('FeedsCategoriesCtrl', function($scope, $http) {
    $scope.feeds_categories = [];

    $http.get('feeds-categories.json').success(function(response) {
        $scope.feeds_categories = response;
    });
})


//bring specific category providers
.controller('CategoryFeedsCtrl', function($scope, $http, $stateParams) {
    $scope.category_sources = [];

    $scope.categoryId = $stateParams.categoryId;

    $http.get('feeds-categories.json').success(function(response) {
        var category = _.find(response, {
            id: $scope.categoryId
        });
        $scope.categoryTitle = category.title;
        $scope.category_sources = category.feed_sources;
    });
})

;